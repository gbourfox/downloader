use reqwest;
use std::fs::File;
use std::io::Write;

const BASE_URL: &str = "https://example.com/";
const PART_COUNT: usize = 999;
const FINAL_FILE_PATH: &str = "file.zip";

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut final_file = File::create(FINAL_FILE_PATH)?;

    for i in 1..=PART_COUNT {
        let filename = format!("file{:03}.part", i);
        let file_url = format!("{}{}", BASE_URL, filename);

        let part_data = loop {
            // Download file
            let response = match reqwest::blocking::get(&file_url) {
                Ok(r) => if r.status().is_success() {
                    Some(r.bytes().unwrap())
                } else {
                    None
                },
                Err(_) => None,
            };
            let response = match response {
                Some(r) => r,
                None => {
                    println!("Failed to download part {:03}, retrying", i);
                    continue;
                },
            };
    
            // Download and verify MD5 checksum
            let md5_filename = format!("{}.md5", filename);
            let md5_url = format!("{}{}", BASE_URL, md5_filename);
            let expected_md5 = reqwest::blocking::get(&md5_url)?
                .text()?
                .trim()
                .to_lowercase(); // Assuming MD5 checksums are in lowercase

            let actual_md5 = format!("{:x}", md5::compute(&response));
    
            if expected_md5 != actual_md5 {
                println!("Checksum verification failed for part {:03}, retrying", i);
            } else {
                break response;
            }
        };
    
        // Write file to the final zip file
        final_file.write_all(&part_data)?;

        println!("Part {:03}/{:03} downloaded and verified successfully.", i, PART_COUNT);
    }

    println!("All files downloaded and verified successfully.");

    Ok(())
}
